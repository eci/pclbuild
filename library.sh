function say {
     python3 -c "import sys;import aux;aux.loglady('$LOGFILE','$LOGWHEN',$LOGHOWMANY,sys.argv)" $@
    }

function header {
    say "[* START *]" $@
}

function footer {
    say "[* END *]" $@
}


function fetch_one_die {
    fn_url="$1"
    (curl -s -f -R -O "$fn_url") || \
		fatal "(fetch): Error: File could not be downloaded from:" $fn_url ".Aborting"
    
}

function fetch_one_warn {
    fn_url="$1"
    (curl -s -f -R -O "$fn_url") || \
		say "(fetch): Warning: File could not be downloaded from:" $fn_url " ."
}


function fetch {
    
    FN_SRC=$(readlink -f "$1")
    status=$?
    DEST_DIR=$2


    [ "${status}" -ne 0 ] && fatal "Error. The file containing the list of files to be
 downloaded does not exist.  "

    cd "${DEST_DIR}"
    say Downloading source files to "${DEST_DIR}"
    while read fn; do
	# Check for md5 hashes
	bfn=$(basename "$fn")	# Base source filename.
	mdfn="${bfn}.md5"	# MD5 filename corresponding to the source filename.
	rm -f "${mdfn}"
	say  "(fetch): Downloading hash file: ${fn}.md5"
	fetch_one_warn "${fn}.md5" # Download MD5, or warn if it does not exist.
	
	if [ -e "$bfn" ] && [ -e "${mdfn}" ]; then
	    
	    md5sum -c --status "$mdfn" && \
		say  "(fetch): Skipping unchanged file $fn ." && \
		     continue
	fi
	
	say  "(fetch): Downloading $fn"
	fetch_one_die "$fn"
	md5sum -c --status "$mdfn" || fatal "(fetch): Error: Download of $fn corrupted. Abort."
    done < "$FN_SRC"

    #exit 0
}



function fetch_wild {
    url=$1
    patt=$2
    dwn=$3
    wget -o wget.log -nd -P "$dwn" -e robots=off -w 1 --random-wait -l1 -r "$url" -A"$patt"
    say "Downloaded to $dwn"
}

function gen_tier_a {

    cidbits=$1
    filterfile=$2
    thescript=$SCRIPTDIR/filter_toc_info.pl
    say CID bit file : "$cidbits" 
    say PubChemLite filter file : "$filterfile"
    say "Script: ${thescript}"
    say "Command: ${DCMPR} < $cidbits | ${thescript} $filterfile 1 | ${CMPR} > ./generated_bits.tsv.gz" 
    header BUILD filter_toc_info '(' $(date) ')'
    ${DCMPR} < "$cidbits" | "${thescript}" "$filterfile" 1 | ${CMPR} > "./generated_bits.tsv.gz"
    [ "$?" -ne 0 ] && fatal "$thescript failed"
    footer BUILD filter_toc_info '(' $(date) ')'
}

function gen_tier_b {
    header BUILD pull_cid_content '(' $(date) ')'
    thescript=$SCRIPTDIR/pull_cid_content.pl
    "${thescript}" "./generated_bits.tsv.gz" > "./generated_data.out"
    [ "$?" -ne 0 ] && fatal "BUILD: pull_cid_content failed"
    footer BUILD pull_cid_content '(' $(date) ')'
}

function gen_tier_c {
    header BUILD rest_grab_props '(' $(date) ')'
    legendfile=$1
    thescript=$SCRIPTDIR/rest_grab_props.pl
    "${thescript}" "$legendfile" < "./generated_data.out" | ${CMPR} > "./generated_data_complete.out.gz"
    [ "$?" -ne 0 ] && fatal "BUILD: rest_grab_props failed"
    footer BUILD rest_grab_props '(' $(date) ')'

}

function gen_tier_d {
    header BUILD remove_unwanted_cases '(' $(date) ')'
    thescript=$SCRIPTDIR/remove_unwanted_cases.pl
    ( ${DCMPR} < "generated_data_complete.out.gz" | "${thescript}" "generated_" > "$OUTMFFILE" )
    [ "$?" -ne 0 ] && fatal "BUILD: rest_unwanted_cases failed"
    footer BUILD remove_unwanted_cases '(' $(date) ')'
}



function gen_tier {
    set -o pipefail		# this will capture a problem that
				# happens while piping
    cidbits=$1
    filterfile=$2
    legendfile=$3
    gen_tier_a "$cidbits" "$filterfile" && \
	gen_tier_b && \
	gen_tier_c "$legendfile" && \
	gen_tier_d
}

function gen_tier_test {
    set -o pipefail		# this will capture a problem that
				# happens while piping
    cidbits=$1
    filterfile=$2
    legendfile=$3
    # gen_tier_c "$legendfile" && \
    # gen_tier_d

}

function stamp {
    header STAMP
    say "Current directory is $PWD"
    say "Input files are located in $INPUTDIR"
    say "The build directory is $WORKDIR"
    say "The filter file used is $FILTERFILE"
    say "The MetFrag file(s) are going to ${MFDIR}"
    say "The output MetFrag file will be $OUTMFFILE"
    say "The scripts have been located in $SCRIPTDIR"
    say "The full build is going to be backed up in $BACKUPDIR"
    say "Legend file is $LEGENDFILE"
    footer STAMP
}

function gen_filtfile  {
    map=$1
    fname=$2


    say "(gen_filtfile) Generating filter file: $fname"
    awk 'BEGIN {FS=","}
         NR==1 {next}
         NR>1 {print($1,$2)}' \
	     "$map" > "$fname"
    
}

function gen_legend {
    map=$1
    fname="$2"

    say "(gen_legend) Generating legend file: $fname"
    awk 'BEGIN {FS=",";OFS=","}
         NR==1 {next}
         NR>1 {print($2,$3)}' \
	     "$map" > "$fname"
}
