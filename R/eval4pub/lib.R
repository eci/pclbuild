setGeneric("length")
setGeneric("run",function(self) {})
setGeneric("write",function(x,file,...) {})

## Logging functions.
logmsg <- function(pref="(PCL Eval)",...) message(pref," ",...)

## Restructure a filename.
tag_fn <- function(fn,tag) {
    if (nchar(fn)==0) stop("Argument fn cannot be an empty string.")
    dir <- dirname(fn)
    parts <- strsplit(basename(fn),split="\\.")[[1]]
    root <- if (length(parts)>1) paste(head(parts,-1),collapse = ".") else parts[[1]]
    ext <- if (length(parts) > 1) paste0(".",tail(parts, 1)) else ""
    if (nchar(tag)!=0) tag <- paste0("_",tag)
    root_tag <- paste0(root,tag)
    file.path(dir,paste0(root_tag,ext))
}

tag_suff_fn <- function(fn_pref,tag) {
    paste0(fn_pref,"_",tag,".csv")
}

logblock <- function(pref="(PCL Eval)",blockname,...) {
    logmsg(pref=pref,"****** BEGIN ", blockname," ******")
    for (line in c(...)) logmsg(pref=pref,line)
    logmsg(pref=pref,"****** END ", blockname, " ******")
}

logme <- function(self,pref="(PCL Eval)") {
    lines <- lapply(slotNames(self),function(nm) paste0(nm,": ",slot(self,nm)))
    do.call(logblock,c(list(pref=pref,
                            blockname=as.character(class(self))),
                       lines))
}
setGeneric("logme")

eval_one <- function(econf,cmpd_info) {
    ## Define global MetFrag parameters first.
    pref = "(PCL Eval check 1)"
    logmsg(pref = pref, "First evaluation started ", Sys.time())
    gprops <- mfglobprops(useMSMS = T,
                          useExactMass = T,
                          pre2020 = F,
                          useMonaIndiv = T,
                          useMoNAMetFusion = F,
                          filter_by_InChIKey = T,
                          runMetFrag = T,
                          verbose = T,
                          extra_terms="none",
                          UDS_Category = "PubMed_Count,Patent_Count,AnnoTypeCount",
                          UDS_Weights = ",1,1,1",
                          ppm = 5,
                          mzabs = 0.001,
                          frag_ppm = 5,
                          i_corr = 0L)

    
    ## Write global parameters to the log.
    logme(gprops, pref = pref)

    ## Create paths.
    make_paths(econf)
    
    ## Create config objects.
    cfg <- mfconfig(econf = econf,
                    cmpd_info = cmpd_info,
                    gprops = gprops)

    ## Run evaluation.
    run(cfg)


    ## Generate summary file.
    cmpd_info <- procit(cfg,
                        econf=econf,
                        cmpd_info,
                        gprops)
    res <- result(cmpd_info)

    ## Write results of the evaluation to the log.
    logme(res, pref = pref)
    
    ## Write results to the quality tracker.
    fn_track <- get_fn_track(x=econf,tag="check_1")
    write(res, file = fn_track, time = econf@time)
    
    logmsg(pref = pref, "First evaluation done ", Sys.time())
    logmsg(pref = pref,"Evaluation file stored in: ", summary_fn(econf))
    
}

eval_two <- function(econf,cmpd_info) {
    ## Define global MetFrag parameters first.
    pref = "(PCL Eval check 2)"
    logmsg(pref = pref, "Second evaluation started ", Sys.time())
    cmpd_info <- cmpd_info[c(1:5,835:837),]
    gprops <- mfglobprops(useMSMS = T,
                          useExactMass = F,
                          pre2020 = F,
                          useMonaIndiv = T,
                          useMoNAMetFusion = F,
                          filter_by_InChIKey = T,
                          runMetFrag = T,
                          verbose = T,
                          extra_terms="none",
                          UDS_Category = "PubMed_Count,Patent_Count,AnnoTypeCount",
                          UDS_Weights = ",1,1,1",
                          ppm = 5,
                          mzabs = 0.001,
                          frag_ppm = 5,
                          i_corr = 0L)

    ## Write global parameters to the log.
    logme(gprops, pref = pref)

    ## Create paths.
    make_paths(econf)
    
    ## Create config objects.
    cfg <- mfconfig(econf = econf,
                    cmpd_info = cmpd_info,
                    gprops = gprops)

    ## Run evaluation.
    run(cfg)


    ## Generate summary file.
    cmpd_info <- procit(cfg,
                        econf=econf,
                        cmpd_info,gprops)
    res <- result(cmpd_info)

    ## Write results of the evaluation to the log.
    logme(res, pref = pref)

    ## Write results to the quality tracker.
    fn_track <- get_fn_track(x=econf,tag="check_2")
    write(res, file = fn_track, time = econf@time)
    
    logmsg(pref = pref, "Second evaluation done ", Sys.time())
    logmsg(pref = pref,"Evaluation file stored in: ", summary_fn(econf))
    
}
