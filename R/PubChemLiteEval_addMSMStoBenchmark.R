# Adding MS/MS to Benchmark for Evaluation Script for PubChemLite
# Emma Schymanski, 18 Nov 2019
# 18 Jan 2020: modifying to go through merged PCID dataset on PCLite 14 Jan 2020.
# at this stage without mass spectral information
# June 2020: functionifying for paper
# Oct 28, 2020: retaining only MS/MS parts (all other functions elsewhere)


# Set up packages
# if (!requireNamespace("BiocManager", quietly = TRUE))
#   install.packages("BiocManager")
# BiocManager::install()

# ##If you don't have the latest RChemMass, reinstall:
# library(devtools)
# install_github("MassBank/RMassBank",dependencies=F)
# install_github("schymane/RChemMass", dependencies = F)
# install_github("schymane/ReSOLUTION", dependencies = F)
library(devtools)
library(RMassBank)
library(ReSOLUTION)
library(RChemMass)

# set up folders
#base_dir <- "C:/DATA/PubChem/PubChemLite/Evaluation/"
base_dir <- getwd()
setwd(base_dir)
# get user_PCLite_eval.R file
user_file_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/pubchemlite/R/user_PCLite_eval.R?inline=false"
download.file(user_file_url,paste0(base_dir,"user_PCLite_eval.R"))
source(paste0(base_dir,"user_PCLite_eval.R"))
# get PCLite_eval_support.R file
user_file_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/pubchemlite/R/PCLite_eval_support.R?inline=false"
download.file(user_file_url,paste0(base_dir,"PCLite_eval_support.R"))
source(paste0(base_dir,"PCLite_eval_support.R"))
# get extractAnnotations.R file
extr_anno_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/annotations/tps/extractAnnotations.R?inline=false"
download.file(extr_anno_url,paste0(base_dir,"extractAnnotations.R"))
source(paste0(base_dir,"extractAnnotations.R"))

## This assumes that MS/MS files are present, see PCLite_eval_prepMetFrag.R

# To prepare files for MetFrag, please see "PCLite_eval_prepMetFrag.R"
file_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/pubchemlite/R/PCLite_eval_prepMetFrag.R"
download.file(file_url, paste0(base_dir,"PCLite_eval_prepMetFrag.R"))

## It should just be these files:
# get the PCLite Benchmarking file needed
zenodo_url <- "https://zenodo.org/record/4146957/files/"
file_url <- paste0(zenodo_url, "PCLite_Benchmark_wMSMS_info.csv")
download.file(file_url, paste0(base_dir,"PCLite_Benchmark_wMSMS_info.csv"))

# get the MS/MS for CASMI / MetFragRL evaluations
file_url <- paste0(zenodo_url,"MSMS.zip")
download.file(file_url,paste0(base_dir,"MSMS.zip"))
unzip(paste0(base_dir,"MSMS.zip"))


# set up files 
msms_dir <- paste0(base_dir,"MSMS/")
# pre MSMS:
#cmpd_file <- paste0(base_dir,"PCLite_EvalSets_mergedPCIDs_wBenchmark.csv")
# the equivalent is cols A:I of this file:
cmpd_file <- paste0(base_dir, "PCLite_Benchmark_wMSMS_info.csv")
file.exists(cmpd_file)


#### adding MS/MS information #####

# recreate this by subsetting cmpd_info
cmpd_info <- read.csv(cmpd_file,stringsAsFactors = F)
cmpd_info <- cmpd_info[,c(1:9)]

cmpd_info$msms_avail <- FALSE
cmpd_info$msms_peaks <- ""
cmpd_info$isPos <- FALSE
use_MSMS <- TRUE
i <- 1
mode_code <- ""
CSID <- ""
PCID <- ""
n_header <- 0


for (i in 1:length(cmpd_info$BenchmarkSet_firstMatch)) {
  msms_filename <- cmpd_info$BenchmarkSet_firstMatch[i]
  msms_file <- list.files(msms_dir,pattern=sub("XX","",msms_filename), 
                          full.names = T,recursive = T)
  msms_code <- strtrim(msms_filename,2)
  mode_code <- ""
  CSID <- ""
  PCID <- ""
  n_header <- 0
  
  if (use_MSMS) {
    msms_file_exists <- file.exists(msms_file)
  } else {
    msms_file_exists <- FALSE
  }

  if (msms_file_exists && msms_code=="XX") {
    #CASMI spectra
    cmpd_info$msms_avail[i] <- TRUE
    msms_peaks <- getMBRecordPeaks(msms_file)[,c(1,2)]
    # write out peaks into cmpd_info
    msms_peak_line <- ""
    for (j in seq_along(msms_peaks[,c(1)])) {
      msms_peak_line <- paste((paste(msms_peaks[j,c(1,2)],collapse=":")),msms_peak_line ,sep=";")
    }
    cmpd_info$msms_peaks[i] <- msms_peak_line
    #check mode
    mode_code <- substr(msms_filename,(nchar(msms_filename)-1),(nchar(msms_filename)-1))
    if (mode_code == "0") {
      cmpd_info$isPos[i] <- TRUE
    }
    
  } else if (msms_file_exists && msms_code=="EA") {
    n_header <- 9
    msms_file_contents <- readLines(msms_file)
    comment_patterns <- c("# Sample: ","# Charge: ","# NeutralPrecursorMass: ",
                          "# Mode: ","## RetentionTime: ","## InchiKey: ",
                          "## ChemSpider: ","## PubChem: ","## Formula: ")
    CSID <- sub(comment_patterns[7],"",
                msms_file_contents[grep(comment_patterns[7],msms_file_contents)])
    PCID <- sub(comment_patterns[8],"",
                msms_file_contents[grep(comment_patterns[8],msms_file_contents)])
    if (length(CSID)>0) {
      #cmpd_info$CSID[i] <- CSID
    } else {
      n_header <- n_header-1
    }
    if (length(PCID)>0) {
      #cmpd_info$PCID[i] <- PCID
    } else {
      n_header <- n_header-1
    }

    #read in the peaks
    cmpd_info$msms_avail[i] <- TRUE
    #msms_peaks <- getMBRecordPeaks(msms_file)[,c(1,2)]
    msms_peaks <- read.table(msms_file,skip=n_header)
    # write out peaks into cmpd_info
    msms_peak_line <- ""
    for (j in seq_along(msms_peaks[,c(1)])) {
      msms_peak_line <- paste((paste(msms_peaks[j,c(1,2)],collapse=":")),msms_peak_line ,sep=";")
    }
    cmpd_info$msms_peaks[i] <- msms_peak_line
    
    #check mode
    mode_code <- substr(msms_filename,(nchar(msms_filename)-4),(nchar(msms_filename)-4))
    if (mode_code == "0") {
      cmpd_info$isPos[i] <- TRUE
    }
    
  } else if (msms_file_exists && msms_code %in% c("EQ","UF")) {
    n_header <- 11
    msms_file_contents <- readLines(msms_file)
    comment_patterns <- c("# Sample: ","# IsPositiveIonMode: ",
                          "# NeutralPrecursorMass: ","# PrecursorIonMode: ",
                          "# SampleName: ","## InchiKey: ","## RetentionTime: ",
                          "## ChemSpider: ","## PubChem: ","## InChI: ",
                          "## NeutralPrecursorMolecularFormula: ")
    CSID <- sub(comment_patterns[8],"",
                msms_file_contents[grep(comment_patterns[8],msms_file_contents)])
    PCID <- sub(comment_patterns[9],"",
                msms_file_contents[grep(comment_patterns[9],msms_file_contents)])
    if (length(CSID)>0) {
      #cmpd_info$CSID[i] <- CSID
    } else {
      n_header <- n_header-1
    }
    if (length(PCID)>0) {
      #cmpd_info$PCID[i] <- PCID
    } else {
      n_header <- n_header-1
    }
    #read in the peaks
    cmpd_info$msms_avail[i] <- TRUE
    msms_peaks <- read.table(msms_file,skip=n_header)
    # write out peaks into cmpd_info
    msms_peak_line <- ""
    for (j in seq_along(msms_peaks[,c(1)])) {
      msms_peak_line <- paste((paste(msms_peaks[j,c(1,2)],collapse=":")),msms_peak_line ,sep=";")
    }
    cmpd_info$msms_peaks[i] <- msms_peak_line
    
    #check mode
    mode_code <- substr(msms_filename,(nchar(msms_filename)-5),(nchar(msms_filename)-5))
    if (mode_code == "0") {
      cmpd_info$isPos[i] <- TRUE
    }
    
  } else { #if (!msms_file_exists)
    cmpd_info$msms_peaks[i] <- NA
    cmpd_info$isPos[i] <- NA
  }
  
}


write.csv(cmpd_info,"PCLite_Benchmark_wMSMS.csv",row.names=F)

# next, fill in the compound info using PubChem functions
cmpd_info$Input_CID <- ""
cmpd_info$Parent_CID <- ""
cmpd_info$Name <- ""
cmpd_info$IUPAC_Name <- ""
cmpd_info$SMILES <- ""
cmpd_info$InChI <- ""
cmpd_info$InChIKey <- ""
cmpd_info$MolecularFormula <- ""
cmpd_info$ExactMass <- ""

for (i in 1:length(cmpd_info$PCID_SMILES)) {
#for (i in 217:length(cmpd_info$PCID_SMILES)) {
    #for (i in na_entries) {
  ms_info <- getMSInfo.cid(cmpd_info$PCID_SMILES[i])
  cmpd_info$Input_CID[i] <- ms_info$Input_CID
  cmpd_info$Parent_CID[i] <- ms_info$Parent_CID
  cmpd_info$Name[i] <- ms_info$Name
  cmpd_info$IUPAC_Name[i] <- ms_info$IUPAC_Name
  cmpd_info$SMILES[i] <- ms_info$SMILES
  cmpd_info$InChI[i] <- ms_info$InChI
  cmpd_info$InChIKey[i] <- ms_info$InChIKey
  cmpd_info$MolecularFormula[i] <- ms_info$MolecularFormula
  cmpd_info$ExactMass[i] <- ms_info$ExactMass
}

# One now has a false preferred CID ... 
na_entries <- which(is.na(cmpd_info$Parent_CID))

for (i in na_entries) {
  ms_info <- getMSInfo.cid(cmpd_info$PCIDs[i])
  cmpd_info$Input_CID[i] <- ms_info$Input_CID
  cmpd_info$Parent_CID[i] <- ms_info$Parent_CID
  cmpd_info$Name[i] <- ms_info$Name
  cmpd_info$IUPAC_Name[i] <- ms_info$IUPAC_Name
  cmpd_info$SMILES[i] <- ms_info$SMILES
  cmpd_info$InChI[i] <- ms_info$InChI
  cmpd_info$InChIKey[i] <- ms_info$InChIKey
  cmpd_info$MolecularFormula[i] <- ms_info$MolecularFormula
  cmpd_info$ExactMass[i] <- ms_info$ExactMass
}


write.csv(cmpd_info,"PCLite_Benchmark_wMSMS_info.csv",row.names=F)


# Legacy file format comments that may be helpful 
# (above script is a cleaned up version)

# CHIN-D-15-00088 EQEx

#set up cmpd_info, 01 are neg, 02 are pos. 
# each file has a header with info ... 
# Sample: EQX284201
# IsPositiveIonMode: True
# NeutralPrecursorMass: 373.10626
# PrecursorIonMode: 1
# SampleName: EQ284201
## InchiKey: FMSOAWSKCWYLBB-VBGLAJCLSA-N
## RetentionTime: 13.1
## ChemSpider: 10770206
## PubChem: 5493381
## InChI: InChI=1S/C21H15N3O4/c25-17-7-3-1-5-15(17)19-22-20(16-6-2-4-8-18(16)26)24(23-19)14-11-9-13(10-12-14)21(27)28/h1-12,25-26H,(H,27,28)
## NeutralPrecursorMolecularFormula: C21H15N3O4


