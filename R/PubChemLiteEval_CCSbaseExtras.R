# draft CCSbase evaluation for PubChemLite. 
# E. Schymanski, 6 Feb 2021
# Builds off CCSbase evaluation results from PubChemLiteEval_merged.R


##### CCS metrics ######
cmpd_info <- read.csv("MetFragResultSummaryPubChemLite_exposomics_CCSbase_XlogP_01Jan21_MergedBenchmark.csv",
                      stringsAsFactors = F)
#cmpd_info <- read.csv(CCS_csv, stringsAsFactors = F)
cmpd_info_colnames <- colnames(cmpd_info)
cmpd_info_colnames[50] <- "cand_pred_CCS_A2_MpHp"
cmpd_info_colnames[51] <- "cand_pred_CCS_A2_MmHm"
colnames(cmpd_info) <- cmpd_info_colnames

cmpd_info$max_CCS_val_MpHp <- ""
cmpd_info$min_CCS_val_MpHp <- ""
cmpd_info$diff_CCS_val_MpHp <- ""
cmpd_info$mean_CCS_val_MpHp <- ""
cmpd_info$max_CCS_val_MmHm <- ""
cmpd_info$min_CCS_val_MmHm <- ""
cmpd_info$diff_CCS_val_MmHm <- ""
cmpd_info$mean_CCS_val_MmHm <- ""

i <- 1
pdf_file <- "CCS_per_Candidate.pdf"
pdf(pdf_file, paper="a4r")

for (i in 1:length(cmpd_info$PCIDs)) {
  #for (i in 1:3) {
  CCS_val_MmHm <- as.numeric(strsplit(cmpd_info$cand_pred_CCS_A2_MmHm[i],";")[[1]])
  max_CCS_val_MmHm <- max(CCS_val_MmHm)
  min_CCS_val_MmHm <- min(CCS_val_MmHm)
  diff_CCS_val_MmHm <- max_CCS_val_MmHm - min_CCS_val_MmHm
  mean_CCS_val_MmHm <- mean(CCS_val_MmHm)
  
  CCS_val_MpHp <- as.numeric(strsplit(cmpd_info$cand_pred_CCS_A2_MpHp[i],";")[[1]])
  max_CCS_val_MpHp <- max(CCS_val_MpHp)
  min_CCS_val_MpHp <- min(CCS_val_MpHp)
  diff_CCS_val_MpHp <- max_CCS_val_MpHp - min_CCS_val_MpHp
  mean_CCS_val_MpHp <- mean(CCS_val_MpHp)
  
  cmpd_info$max_CCS_val_MpHp[i] <- max_CCS_val_MpHp
  cmpd_info$min_CCS_val_MpHp[i] <- min_CCS_val_MpHp
  cmpd_info$diff_CCS_val_MpHp[i] <- diff_CCS_val_MpHp
  cmpd_info$mean_CCS_val_MpHp[i] <- mean_CCS_val_MpHp
  cmpd_info$max_CCS_val_MmHm[i] <- max_CCS_val_MmHm
  cmpd_info$min_CCS_val_MmHm[i] <- min_CCS_val_MmHm
  cmpd_info$diff_CCS_val_MmHm[i] <- diff_CCS_val_MmHm
  cmpd_info$mean_CCS_val_MmHm[i] <- mean_CCS_val_MmHm
  
  main_title <- paste0("BM_PCID: ", cmpd_info$PCIDs[i],
                       " : Predicted CCS for [M+H]+ and [M-H]-")
  
  plotCCS(CCS_val_MpHp,CCS_val_MmHm,main_title)
  
}
dev.off()
summary_csv_name <- "MetFragResultSummaryPubChemLite_exposomics_CCSbase_XlogP_01Jan21_MergedBM_wCCSstats.csv"
write.csv(cmpd_info,summary_csv_name,row.names = F)

### now add XlogP info


cmpd_info$max_XlogP_val <- ""
cmpd_info$min_XlogP_val <- ""
cmpd_info$diff_XlogP_val <- ""
cmpd_info$mean_XlogP_val <- ""

i <- 5
pdf_file <- "XlogP_per_Candidate.pdf"
pdf(pdf_file, paper="a4r")

for (i in 1:length(cmpd_info$PCIDs)) {
  #for (i in 1:3) {
  XlogP_val <- as.numeric(strsplit(cmpd_info$cand_XlogP[i],";")[[1]])
  max_XlogP_val <- max(XlogP_val, na.rm = TRUE)
  min_XlogP_val <- min(XlogP_val, na.rm = TRUE)
  diff_XlogP_val <- max_XlogP_val - min_XlogP_val
  mean_XlogP_val <- mean(XlogP_val, na.rm = TRUE)
  
  cmpd_info$max_XlogP_val[i] <- max_XlogP_val
  cmpd_info$min_XlogP_val[i] <- min_XlogP_val
  cmpd_info$diff_XlogP_val[i] <- diff_XlogP_val
  cmpd_info$mean_XlogP_val[i] <- mean_XlogP_val

  main_title <- paste0("BM_PCID: ", cmpd_info$PCIDs[i],
                       " : Predicted XlogP")
  
  XlogP_val <- as.numeric(XlogP_val)
  plotXlogP(XlogP_val,main_title)
  
}
dev.off()
summary_csv_name <- "MetFragResultSummaryPubChemLite_exposomics_CCSbase_XlogP_01Jan21_MergedBM_wCCSstats.csv"
write.csv(cmpd_info,summary_csv_name,row.names = F)

# plotting CCS and XlogP together
cmpd_info <- read.csv("MetFragResultSummaryPubChemLite_exposomics_CCSbase_XlogP_01Jan21_MergedBM_wCCSstats_expCCS.csv",
                      stringsAsFactors = F)

i <- 5
pdf_file <- "CCS_and_XlogP_per_Candidate_Top20_wExpCCS.pdf"
#pdf_file <- "CCS_and_XlogP_per_Candidate_Top20.pdf"
#pdf_file <- "CCS_and_XlogP_per_Candidate.pdf"
pdf(pdf_file, paper="a4r")

for (i in 1:length(cmpd_info$PCIDs)) {
  #for (i in 1:3) {
  CCS_val_MmHm <- as.numeric(strsplit(cmpd_info$cand_pred_CCS_A2_MmHm[i],";")[[1]])
  CCS_val_MpHp <- as.numeric(strsplit(cmpd_info$cand_pred_CCS_A2_MpHp[i],";")[[1]])
  if (cmpd_info$isPos[i]) {
    CCS_val <- CCS_val_MpHp
    exp_CCS <- cmpd_info$Exp_CCS_MpHp[i]
  } else {
    CCS_val <- CCS_val_MmHm
    exp_CCS <- cmpd_info$Exp_CCS_MmHm[i]
  }
  XlogP_val <- as.numeric(strsplit(cmpd_info$cand_XlogP[i],";")[[1]])

  main_title <- paste0("BM_PCID: ", cmpd_info$PCIDs[i],
                       " : Pred. CCS & XlogP (Top 20) Rank=",
                       cmpd_info$IKeyFBlock_rank[i])
  
  if (is.na(exp_CCS)) {
    plotCCSnXlogP(CCS_val,XlogP_val,main_title,topN=20)
  } else {
    plotCCSnXlogP(CCS_val,XlogP_val,main_title,topN=20,exp_CCS = exp_CCS)
  }
  
}
dev.off()
