import argparse
import os
import sys
import pathlib

import aux

from . import lib
from .consts import *



# Get logger.


log = aux.logger()



# We want to throw errors so that I can predictably log them.
class MyArgumentParser(argparse.ArgumentParser):
    def exit(self, status=0, message=None):
        if status:
            raise Exception(f'Exiting because of an error: {message}')
        exit(status)

parser = MyArgumentParser(description='Download input files for PubChemLite build if they changed on the server.',epilog="The `getter' module checks if the files with urls specified in the `sources.txt' (in INPUT) differ from those in the TO directory. If they do, `getter' will download the changed files.")


parser.add_argument('--logs','-l',help="Directory which should contain logs. If not specified, logs will be created in logs subdirectory of the current working directory.",default=None)
parser.add_argument('--input', '-i',
                    help="URL of a git repo containing configuration files for `getter' (such as sources.txt) .",
                    required=True)
parser.add_argument('--to','-t',help="Destination for download of PubChemLite input files.",required=True)
parser.add_argument('--retry','-r',help="Duration of the pause between attempts to redownload of an input file in seconds, after a download error.",required=True)




def main(args):
    """This is the entry point for the getter package.

       The first argument in the `args` list is the `input` directory
    and the second is the download directory.

    """

    dir_dwnl = args.to
    retry_in_sec = args.retry


    
    try:
        
        assert isinstance(args.input,str), "Input directory must be a string."
        assert isinstance(dir_dwnl,str), "Download directory must be a string."
        assert isinstance(retry_in_sec,str), "Retry interval must be a string."
    except AssertionError as e:
        log.critical(e)
        exit(1)


    dir_input = os.path.join(dir_dwnl,DIR_INPUT)
    dir_input = pathlib.Path(dir_input).resolve()

    # Get input.
    aux.clone(dest=dir_input,url=args.input)

        
    try:
        msg = "Input directory {0} does not exist.".format(dir_input)
        assert os.path.exists(dir_input), msg
    except AssertionError as e:
        log.critical(e)
        exit(1)

    fn_sources = os.path.join(dir_input,FN_SOURCES)
    try:
        msg = "Input file {0} does not exist.".format(fn_sources)
        assert os.path.exists(fn_sources), msg
    except AssertionError as e:
        log.critical(e)
        exit(1)


    lib.run(fn_sources,dir_dwnl,retry_in_sec)
        
        


args = parser.parse_args()

# Initialise the logger.
log = aux.conf_logger(logr=log,dir_log=args.logs,
                      fn_log=FN_LOG,descriptor='getter',
                      when=LOG_WHEN,atTime=LOG_ATTIME,
                      backupCount=LOG_HOWMANY)

    
main(args)
