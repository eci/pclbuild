# PubChemLite Build System

[[_TOC_]]

## Overview

The PubChemLite Build System (referred to as `pclbuild` further in
text) provides a kind of informal _continuous integration_ tool for
creation of PubChemLite databases.

For more information on PubChemLite databases, please refer to ... **TODO**. 

It has been written with a team of two types of users in mind. One is
a **build administrator** who deploys the build system onto a POSIX,
usually GNU/Linux machine. The other type is a **database designer**,
the one who decides which parts of PubChem database are supposed to
end up in its *-lite* version.

The instructions are going to be written separately for both user
personalities. 

## How-To for the Database Designer

The responsibility of the database designer:

- Selecting the parts of PubChem which enter PubChemLite.
- Defining the location of various outputs (in coordination with the build admin).
- Providing benchmarking inputs.
- Making sure that the low-level scripts used for building are
  up-to-date (when PubChem team updates theirs).
- Curating the lists of build sources.

In order to acomplish these tasks, the designer creates an `input` git
repository which contains various files which inform the build system
how the database should be built.

### Input Git Repository

The repository should contain the following files:

- The YAML manifest (`manifest.yaml`).
- The PubChemLite map file (`PubChemLite_exposomics.map`).
- The PubChem database sources files (`sources.txt`)

#### YAML Manifest

Here are the contents of an example `manifest.yaml`.

```{yaml}
# The file containing index bits of PubChem, the PubChem category
# names and the corresponding metfrag column names.
map: PubChemLite_exposomics.map

# Where to store the entire build (for backup and forensics). This 
# location is relative to the directory in which the build started.
top_backup: share/CompoundDBs/PubChem/PubChemLite/pcl_v2.0_test/backups

# URL of the eval4pub benchmark file.
url_bench: "https://zenodo.org/record/4146957/files/PCLite_Benchmark_wMSMS_info.csv"

# Directory, or directories where to store output MetFrag files. This location 
# is relative to the directory in which the build started.
metfrag: share/CompoundDBs/PubChem/PubChemLite/pcl_v2.0_test/MetFrag_CSVs


# Path to the CCS file.
ccs_src_path: "share/CompoundDBs/PubChem/PubChemLite/ccs_input/ccs-pclite-comput-output.csv.gz"
```

#### PubChemLite Map File

This file is used to filter the PubChem content. Here is an example of
`PubChemLite_exposomics.map`.

```
BIT,CATEGORY,MFCOL
192,Agrochemical Information,AgroChemInfo
577,Interactions and Pathways,BioPathway
82,Drug and Medication Information,DrugMedicInfo
204,Food Additives and Ingredients,FoodRelated
344,Pharmacology and Biochemistry,PharmacoInfo
356,Safety and Hazards,SafetyInfo
396,Toxicity,ToxicityInfo
350,Use and Manufacturing,KnownUse
137,Associated Disorders and Diseases,DisorderDisease
171,Identification,Identification
```

The first column are the category bits which will select PubChem
categories to enter the PubChemLite database. The second column are
the PubChem names of these categories. The last column are the column
names in the resulting MetFrag database corresponding to the PubChem
categories. 

All PubChem bits and the categories can be found in
[index-current.tab](https://ftp.ncbi.nlm.nih.gov/pubchem/.toc_fp/index-current.tab).

#### PubChem Database Sources Files

The file `sources.txt` lists the locations of the PubChem databases.

```
https://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-PMID.gz
https://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-Patent.gz
https://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-Parent.gz
https://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-InChI-Key.gz
https://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-Synonym-filtered.gz
https://ftp.ncbi.nlm.nih.gov/pubchem/.toc_fp/cid-bits-current.tab.gz
https://ftp.ncbi.nlm.nih.gov/pubchem/.toc_fp/index-current.tab
```

#### Updating Build Scripts

PubChem build scripts are found in the root of `pclbuild` git
repository. If the PubChem team updates their one-off build scripts,
the database designer should adapt shell function `build` in
[pb-lite-driver.sh](pb-lite-driver.sh), the shell script
[generate](generate) and the shell functions `gen_tier_*` in
[library.sh](library.sh), as well as all the scripts provided by
PubChem currently in this repository.
