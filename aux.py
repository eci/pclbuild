import datetime
import logging
import logging.handlers
import os
import shutil
import subprocess
import sys
import tempfile
import urllib.parse
import urllib.request

def logger():
    return(logging.getLogger('pclbuild'))


log = logger()
    
def clone(dest,url=None):
    """If URL is a HTTP(S) address of a git repository, retrieve the content. If it is a path, check if it is a git repository, update with the latest changes, otherwise just continue."""

    import sys
    
    url_ok = False
    is_git = False
    to_replace = False
    

    # Clone to a temp folder.
    try:
        os.environ['GIT_TERMINAL_PROMPT']='0'
        tdir = tempfile.mkdtemp()
        subprocess.run(['git','clone',url,tdir],check=True)
        to_replace = True
    except Exception as e:
        log.warning("Unable to clone from {url} to {dir}. Will proceed with the possibly outdated inputs in {dest}.")

    # Remove DEST if conditions met.
    if to_replace:
        try:
            if os.path.exists(dest):
                shutil.rmtree(dest)
            shutil.copytree(tdir,dest)
            shutil.rmtree(tdir)
        except Exception as e:
            log.critical(f"Unable to move {tdir} to {dest}.")
            exit(1)


    # Record the commit.
    orig = os.getcwd()
    try:

        try:
            os.chdir(dest)
        except Exception as e:
            log.critical(f"Unable to switch to DEST {dest}")
            exit(1)

        res = subprocess.run(["git","log","-n1","--pretty='%aI %H'"],check=True,text=True,capture_output=True)
        log.info(f"Input destination: {url}")
        log.info(f"Input commit: {res.stdout}")
        
    except Exception as e:
        log.critical(f"Unable to record the commit: {e}")
        
    finally:
        os.chdir(orig)


        
        
def conf_logger(logr,dir_log,fn_log,descriptor,when,backupCount,atTime=None):

    logr.setLevel(logging.DEBUG)

    if not dir_log:
        dir_log = os.path.join(os.getcwd(),'logs')

    os.makedirs(dir_log, exist_ok=True)
    
    fn_log = os.path.join(dir_log,fn_log)
    
    # Add the log message handler to the logger.
    handler = logging.handlers.TimedRotatingFileHandler(
        fn_log, when=when,
        atTime=atTime,backupCount=backupCount)

    # Formatter.
    formatter = logging.Formatter(f'({descriptor}):%(asctime)s:%(levelname)s:%(message)s')
    handler.setFormatter(formatter)
    
    logr.addHandler(handler)

    return(logr)


def loglady(fn_log,when,backupCount,args,atTime=datetime.time(0,0,0)):
    dir_log = os.path.dirname(fn_log)
    fn_log = os.path.basename(fn_log)
    log = conf_logger(logr=logger(),dir_log=dir_log,
                      fn_log=fn_log,descriptor='LOGLADY',
                      when=when,atTime=atTime,
                      backupCount=backupCount)

    msg = ' '.join(args[1:])
    log.info(msg)
    return(None)

    
    
        
    
