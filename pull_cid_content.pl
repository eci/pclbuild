#!/usr/bin/perl

use strict;
use warnings;

my $file = $ARGV[0];

print STDERR ":: Using input file: \"$file\"\n";

#
#lrwxrwxrwx 1 bolton chemistry        45 Oct 17 03:57 CID-Parent.gz -> /am/ftp-pubchem/Compound/Extras/CID-Parent.gz
#lrwxrwxrwx 1 bolton chemistry        48 Oct 17 03:57 CID-InChI-Key.gz -> /am/ftp-pubchem/Compound/Extras/CID-InChI-Key.gz
#lrwxrwxrwx 1 bolton chemistry        43 Oct 17 03:56 CID-PMID.gz -> /am/ftp-pubchem/Compound/Extras/CID-PMID.gz
#lrwxrwxrwx 1 bolton chemistry        45 Oct 17 03:57 CID-Patent.gz -> /am/ftp-pubchem/Compound/Extras/CID-Patent.gz
#lrwxrwxrwx 1 bolton chemistry        55 Oct 21 14:19 CID-Synonym-filtered.gz -> /am/ftp-pubchem/Compound/Extras/CID-Synonym-filtered.gz

my $failed = 0;
print STDERR "Reading fingerprint data...  (using file: $file)\n";
my %u = ();
open( LST, "gunzip < $file |" ) || die "Unable to read $file!\n";
$_ = <LST>;
$_ = <LST>;
my $nffp = 0;
while ( $_ = <LST> ) {
  chop;
  my ( $cid, $fp ) = split( /	/, $_, 2 );
  $u{ $cid } = $fp;

  if ( $nffp == 0 ) {
    my @tmp = split( /	/, $fp );
    $nffp = @tmp;
  }
}
close( LST );
my $ncid = keys( %u );
print STDERR "$ncid unique CIDs\n";


print STDERR "Getting a single best name for the CIDs  (using file: CID-Synonym-filtered.gz)\n";
my $prev = 0;
my %un = ();
my $nline = 0;
my $nncid = 0;
open ( NAM, "/usr/bin/gunzip < CID-Synonym-filtered.gz |" ) || die "Unable to read CID-Synonym-filtered.gz!\n";
while ( $_ = <NAM> ) {
  $nline++;
  chop;
  my( $c, $n ) = split( /	/, $_, 2 );

  if ( $c != $prev ) { # Get first name from each CID (as it is the best name)
    $prev = $c;
    $nncid++;

    if ( defined( $u{ $c } ) ) {
      $un{ $c } = $n;
    }
  }
}
close( NAM );
my $nun = keys( %un );
print STDERR "Located $nun names for $ncid CIDs after reading $nline CID-name associations from $nncid CIDs\n";


#lrwxrwxrwx 1 bolton chemistry        45 Oct 17 03:57 CID-Parent.gz -> /am/ftp-pubchem/Compound/Extras/CID-Parent.gz
my %v = ();
my %vv = ();
print STDERR "Reading parent data... (using file: CID-Parent.gz)\n";
open( PAR, "gunzip < CID-Parent.gz |" ) || die "Unable to read CID-Parent.gz!\n";
while( $_ = <PAR> ) {
  chop;
  my ( $cid, $pid ) = split( /	/, $_, 2 );
  if ( defined( $u{ $cid } ) ) {
    if ( $pid eq "" || $pid == 0 ) {
      $v{ $cid }{ $cid } = "";
      $vv{ $cid }{ $cid } = "";
    } else {
      $v{ $pid }{ $cid } = "";
      $vv{ $cid }{ $pid } = "";
    }
  }
}
close( PAR );

print STDERR "Backfilling CIDs without parent...\n";
my $nbf = 0;
foreach my $cid ( keys( %u ) ) {
  if ( ! defined( $vv{ $cid } ) ) {
    $v{ $cid }{ $cid } = "";  # Create "self" parent record
    $nbf++;
  }
}
my $npid = keys( %v );
print STDERR "$npid unique parent CIDs ($nbf backfilled)\n";
%vv = ();  # Free memory
undef %vv;


#lrwxrwxrwx 1 bolton chemistry        48 Oct 17 03:57 CID-InChI-Key.gz -> /am/ftp-pubchem/Compound/Extras/CID-InChI-Key.gz
my %i = ();
my %ii = ();
print STDERR "Reading InChIKey data...  (using file: CID-InChI-Key.gz)\n";
open( INC, "gunzip < CID-InChI-Key.gz |" ) || die "Unable to read CID-InChI-Key.gz!\n";
while( $_ = <INC> ) {
  chop;
  my ( $cid, $inchi, $key ) = split( /	/, $_, 3 );
  if ( defined( $v{ $cid } ) ) {
    my ( $f, $s, $t ) = split( /-/, $key, 3 );
    $i{ $f }{ $cid } = "";
    $ii{ $cid }{ $f } = "";
  }
}
close( INC );
my $nfb = keys( %i );
print STDERR "$nfb unique InChIKey first block values\n";

print STDERR "  Checking for missing InChIKey...\n";
my $nmiss = 0;
foreach my $cid ( keys( %v ) ) {
  if ( ! defined( $ii{ $cid } ) ) {
    print STDERR "    parent CID $cid (",
                 join( " ", sort( keys( %{ $v{ $cid } } ) ) ), ") missing InChIKey\n";
    $nmiss++;
  }
}
if ( $nmiss != 0 ) { print STDERR "    $nmiss parent CIDs are missing InChIKey!\n"; }

print STDERR "  Checking for many InChIKey to one parent CID associations...\n";
my $nmult = 0;
foreach my $cid ( keys( %ii ) ) {
  my $n = keys( %{ $ii{ $cid } } );
  if ( $n != 1 ) {
    print STDERR "    parent CID $cid  has $n InChIKey first blocks (",
                 join( " ", sort( keys( %{ $ii{ $cid } } ) ) ), ")\n";
    $nmult++;
  }
}
if ( $nmult != 0 ) { print STDERR "    $nmult parent CIDs have multiple InChIKey first blocks!\n"; }
%ii = ();  # Free memory
undef %ii;



#lrwxrwxrwx 1 bolton chemistry        43 Oct 17 03:56 CID-PMID.gz -> /am/ftp-pubchem/Compound/Extras/CID-PMID.gz
my %p = ();
print STDERR "Creating PMID to CID association...  (using file: CID-PMID.gz)\n";
open( PMID, "gunzip < CID-PMID.gz |" ) || die "Unable to read CID-PMID.gz!\n";
while( $_ = <PMID> ) {
  chop;
  my ( $cid, $pmid, $type ) = split( /	/, $_, 3 );
  if ( defined( $u{ $cid } ) || defined( $v{ $cid } ) ) {
    $p{ $cid }{ $pmid } = "";
  }
}
close( PMID );
my $npc = keys( %p );
print STDERR "$npc CID with PMIDs\n";

my %pp = ();
print STDERR "Creating PMID to parent CID association...\n";
foreach my $pid ( keys( %v ) ) {
  # Get the PMIDs for parent CID
  if ( defined( $p{ $pid } ) ) {
    foreach my $pmid ( keys( %{ $p{ $pid } } ) ) {
      $pp{ $pid }{ $pmid } = "";
    }
  }

  # Get PMIDs for parent associated CIDs
  foreach my $cid ( keys( %{ $v{ $pid } } ) ) {
    if ( defined( $p{ $cid } ) ) {
      foreach my $pmid ( keys( %{ $p{ $cid } } ) ) {
        $pp{ $pid }{ $pmid } = "";
      }
    }
  }
}
my $nppc = keys( %pp );
print STDERR "$nppc parent CID with PMIDs\n";
%p = ();  # Free memory
undef %p;

my %ppp = ();
print STDERR "Creating PMID to InChIKey first block association...\n";
foreach my $key ( keys( %i ) ) {
  foreach my $pid ( keys( %{ $i{ $key } } ) ) {
    foreach my $pmid ( keys( %{ $pp{ $pid } } ) ) {
      $ppp{ $key }{ $pmid } = "";
    }
  }
}
my $npppc = keys( %ppp );
print STDERR "$npppc InChIKey first block with PMIDs\n";
%pp = ();  # Free memory
undef %pp;


#lrwxrwxrwx 1 bolton chemistry        45 Oct 17 03:57 CID-Patent.gz -> /am/ftp-pubchem/Compound/Extras/CID-Patent.gz
my %o = ();
print STDERR "Creating Patent to CID association...  (using file: CID-Patent.gz)\n";
open( PPID, "gunzip < CID-Patent.gz |" ) || die "Unable to read CID-Patent.gz!\n";
while( $_ = <PPID> ) {
  my ( $cid, $ppid ) = split( /	/, $_, 2 );
  if ( defined( $u{ $cid } ) || defined( $v{ $cid } ) ) {
    $o{ $cid }{ $ppid } = "";
  }
}
close( PPID );
my $noc = keys( %o );
print STDERR "$noc CIDs with Patent IDs\n";

my %oo = ();
print STDERR "Creating Patent to parent CID association...\n";
foreach my $pid ( keys( %v ) ) {
  # Get the PPIDs for parent CID
  if ( defined( $o{ $pid } ) ) {
    foreach my $ppid ( keys( %{ $o{ $pid } } ) ) {
      $oo{ $pid }{ $ppid } = "";
    }
  }

  # Get PPIDs for parent associated CIDs
  foreach my $cid ( keys( %{ $v{ $pid } } ) ) {
    if ( defined( $o{ $cid } ) ) {
      foreach my $ppid ( keys( %{ $o{ $cid } } ) ) {
        $oo{ $pid }{ $ppid } = "";
      }
    }
  }
}
my $nooc = keys( %oo );
print STDERR "$nooc parent CIDs with Patent IDs\n";
%o = ();  # Free memory
undef %o;

my %ooo = ();
print STDERR "Creating Patent to InChIKey first block association...\n";
foreach my $key ( keys( %i ) ) {
  foreach my $pid ( keys( %{ $i{ $key } } ) ) {
    foreach my $ppid ( keys( %{ $oo{ $pid } } ) ) {
      $ooo{ $key }{ $ppid } = "";
    }
  }
}
my $noooc = keys( %ooo );
print STDERR "$noooc InChIKey first blocks with Patent IDs\n";
%oo = ();  # Free memory
undef %oo;


print STDERR "Creating merged fingerprint for parent CID...\n";
my %uu = ();
foreach my $pid ( keys( %v ) ) {
  my @cids = keys( %{ $v{ $pid } } );
  my $ncids = @cids;
  if ( $ncids == 1 ) {
    $uu{ $pid } = $u{ $cids[ 0 ] };
    next;  # Nothing to merge .. just one CID
  }

  # Get unique set of FP signatures
  my %fp = ();
  foreach my $cid ( @cids ) {
    $fp{ $u{ $cid } } = "";
  }

  my @fp = keys( %fp );
  my $nfp = @fp;
  if ( $nfp == 1 ) {
    $uu{ $pid } = $fp[ 0 ];
    next;  # Nothing to merge .. just one unique FP
  }

#  print "FPs to merge:\n";
#  for ( my $i = 0;  $i < $ncids;  $i++ ) {
#    print "  $cids[$i]\t$u{$cids[$i]}\n";
#  }

  # Convert array of text FP into array of arrays
  my @tffp = ();
  for ( my $i = 0;  $i < $nfp;  $i++ ) {
    my @ffp = split( /	/, $fp[ $i ] );
    $tffp[ $i ] = [ @ffp ];
  }

  # Loop over the unique fingerprint sets
  my @tfp = ();
  for ( my $ii = 0;  $ii < $nffp;  $ii++ ) {  # Loop over FP blocks
    my $tfp = "";
    my $nl = length( $tffp[ 0 ][ $ii ] );  # Get length of current FP block being merged

    for ( my $j = 0; $j < $nl;  $j++ ) {  # Loop over bits in this block
      my $v = "0";
      for ( my $i = 0; $i < $nfp;  $i++ ) {  # Loop over unique FPs in this block
        my $tv = substr( $tffp[ $i ][ $ii ], $j, 1 );
        if ( $tv eq "1" ) {
          $v = "1";
          last;
        }
      }
      $tfp .= $v;
    }

    $tfp[ $ii ] = $tfp;
  }

  $uu{ $pid } = join( "	", @tfp );
#print "Merged FP:  $uu{$pid}\n";
}
my $nuu = keys( %uu );
print STDERR "$nuu merged FP by parent CID\n";
#%u = ();
#undef %u;


print STDERR "Creating merged fingerprint for InChIKey first block...\n";
my %uuu = ();
foreach my $i ( keys( %i ) ) {
  my @cids = keys( %{ $i{ $i } } );
  my $ncids = @cids;
  if ( $ncids == 1 ) {
    $uuu{ $i } = $uu{ $cids[ 0 ] };
    next;  # Nothing to merge .. just one CID
  }
#print "#CIDs: $ncids .. ", join( " ", @cids ), "\n";

  my %fp = ();
  foreach my $cid ( @cids ) {
    if ( ! defined( $uu{ $cid } ) ) {
     print STDERR "Yikes!  CID $cid is missing parent CID\n";
     $failed = 1;
    } else {
      $fp{ $uu{ $cid } } = "";
    }
  }

  my @fp = keys( %fp );
  my $nfp = @fp;
  if ( $nfp == 1 ) {
    $uuu{ $i } = $fp[ 0 ];
    next;  # Nothing to merge .. just one unique FP
  }

#  print "FPs to merge:\n";
#  for ( my $i = 0;  $i < $ncids;  $i++ ) {
#    print "  $cids[$i]\t$uu{$cids[$i]}\n";
#  }

  # Convert array of text FP into array of arrays
  my @tffp = ();
  for ( my $i = 0;  $i < $nfp;  $i++ ) {
    my @ffp = split( /	/, $fp[ $i ] );
    my $tnffp = @ffp;
    if ( $nffp != $tnffp ) {
      print "Yikes! .. $nffp != $tnffp for $i of $nfp\n";
      $failed = 1;
    }

    $tffp[ $i ] = [ @ffp ];
  }

  # Loop over the unique fingerprint sets
  my @tfp = ();
  for ( my $ii = 0;  $ii < $nffp;  $ii++ ) {  # Loop over FP blocks
    my $tfp = "";
    my $nl = length( $tffp[ 0 ][ $ii ] );  # Get length of current FP block being merged

    for ( my $j = 0; $j < $nl;  $j++ ) {  # Loop over bits in this block
      my $v = "0";
      for ( my $i = 0; $i < $nfp;  $i++ ) {  # Loop over unique FPs in this block
my $tnl = length( $tffp[ $i ][ $ii ] );
if ( $nl != $tnl ) {
  print "Yikes! .. $nl != $tnl .. $ii of $nffp .. $j of $nl .. $i of $nfp\n";
}
        my $tv = substr( $tffp[ $i ][ $ii ], $j, 1 );
        if ( $tv eq "1" ) {
          $v = "1";
          last;
        }
      }
      $tfp .= $v;
    }

    $tfp[ $ii ] = $tfp;
  }

  $uuu{ $i } = join( "	", @tfp );
#print "Merged FP:  $uuu{$i}\n";
}
my $nuuu = keys( %uuu );
print STDERR "$nuuu merged FP by InChIKey first block\n";


print STDERR "Picking best parent CID for InChIKey first block...\n";
my %ip = ();
foreach my $i ( keys( %i ) ) {
  # Check if more than one associated parent CIDs (if so, pick one)
  my @pcid = sort bynum( keys( %{ $i{ $i } } ) );
  my $npcid = @pcid;
  my $pid = 0;
  if ( $npcid == 0 ) {
    print STDERR "InChI block \"$i\" missing parent CID!\n";
    next;
  } elsif ( $npcid == 1 ) {
    $ip{ $i } = $pcid[ 0 ];
  } else {
    my $pid = $pcid[ 0 ];
    my $bfp = $uu{ $pid };
    my $nl = length( $bfp );
    my $nbfpb = 0;
    for ( my $j = 0; $j < $nl;  $j++ ) {
      my $tv = substr( $bfp, $j, 1 );
      if ( $tv eq "1" ) { $nbfpb++; }
    }

    # Pick FP with most bits set
    for ( my $i = 1;  $i < $npcid;  $i++ ) {
      my $tfp = $uu{ $pcid[ $i ] };
      my $ntfpb = 0;
      for ( my $j = 0; $j < $nl;  $j++ ) {
        my $tv = substr( $tfp, $j, 1 );
        if ( $tv eq "1" ) { $ntfpb++; }
      }

      if ( $ntfpb > $nbfpb ) {  # This CID has more bits set
        $nbfpb = $ntfpb;
        $pid = $pcid[ $i ];
      }
    }

    # InChIKey first block to parent CID representative
    $ip{ $i } = $pid;
  }
}
%uu = ();  # Free memory
undef %uu;


print STDERR "Outputing final tabular content by InChIKey first block...\n";
foreach my $i ( keys( %i ) ) {
  if ( ! defined( $ip{ $i } ) ) {
    print STDERR "InChI block \"$i\" missing representive CID!\n";
    next;
  }

  my $pid = $ip{ $i };

  # Get unique list of CIDs assoicated to InChI first block
  my %b = ();
  foreach my $tpid ( keys( %{ $i{ $i } } ) ) {
    foreach my $tcid ( keys( %{ $v{ $tpid } } ) ) {
      $b{ $tcid } = "";
    }
  }
  my $ncids = keys( %b );
  if ( $ncids == 0 ) {
    print STDERR "Yikes!  InChI block \"$i\" missing CIDs!\n";
    next;
  }

  my $n = "";
  if ( defined( $un{ $pid } ) ) {
    $n = $un{ $pid };
  } else {
    # Best parent CID does not have a CID .. look over other CIDs and pick first best name
    foreach my $cid ( sort bynum( keys( %b ) ) ) {
      if ( defined( $un{ $cid } ) ) {
        $n = $un{ $cid };
        last;
      }
    }

    if ( $n eq "" ) {
      print STDERR "InChI block \"$i\" representive CID $pid is missing chemical name! (",
                   join( ", ", sort bynum( keys( %b ) ) ), ")\n";
    }
  }


  # Get the fingerpring
  if ( ! defined( $uuu{ $i } ) ) {
    print STDERR "InChI block \"$i\" missing fingerprint!\n";
    next;
  }

  my $fp = "";
  {
    my $fps = "";
    my $tot_fp = 0;
    my $fpb = $uuu{ $i };

    # Create FP composite block (first bit from each FP block)
    my @tfp = split( /	/, $fpb );
    foreach my $tfp ( @tfp ) {
      my $b = substr( $tfp, 0, 1 );  # first bit
      $fps .= $b;

      if ( $b eq "1" ) { $tot_fp++; }
    }

    my @fpb = ();
    my $k = 0;
    foreach my $tfp ( @tfp ) {
      my $len = length( $tfp );
      $fpb[ $k ] = 0;
      for ( my $i = 0;  $i < $len;  $i++ ) {
        my $b = substr( $tfp, $i, 1 );
        if ( $b eq "1" ) {  $fpb[ $k ]++; }
      }
      $k++;
    }

    $fp = join( "	", $fps, $tot_fp, @fpb );
  }


  # Get unique PMID count
  my $npmid = 0;
  if ( defined( $ppp{ $i } ) ) {
    $npmid = keys( %{ $ppp{ $i } } );
  }

  # Get unique Patent count
  my $nppid = 0;
  if ( defined( $ooo{ $i } ) ) {
    $nppid = keys( %{ $ooo{ $i } } );
  }

  # Get CID list represented by InChIKey first block
  my $cids = join( " ", sort bynum( keys( %b ) ) );

  print "$pid\t$i\t$fp\t$npmid\t$nppid\t$cids\t$n\n";
}


sub bynum { $a <=> $b };

