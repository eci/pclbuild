(use-modules (guix packages)
	     (gnu packages autotools)
	     (gnu packages gcc)
	     (gnu packages perl)
	     (gnu packages mpi)
	     (gnu packages ssh)
	     (gnu packages gawk)
	     (gnu packages base))



(define specs '("bash"
		"sbc"
		"openssh"
		"git:send-email"
		"git"
		"grep"
                "sed"
                "gawk"
		"procps"
		"coreutils-minimal"
		"glibc-locales"
		"emacs"
                "lesspipe"
                "glibc-locales"
		"guile"
		"pkg-config"
		"nss-certs"
		"wget"
		"xz"
		"tar"
		"gzip"
		"bzip2"))

(define build-systems '("automake"
			"autoconf"
			"make"
			"meson"
			"ninja"
			"cmake"))


(define misc-specs '("ispell"
		     "sshpass"
		     ;; "the-silver-searcher"
		     "ripgrep"
		     "xdg-utils"
		     "xdg-user-dirs"
		     "pango"
		     "gnupg"
		     "openssl"
		     "gtk+"
		     "man-db"
		     "texinfo"))


(define font-specs '("unicode-emoji"
		     "fontconfig"
		     "font-dejavu"
		     "font-gnu-freefont"
		     ;"font-ubuntu"
		     "font-terminus"
		     "font-liberation"
		     "font-inconsolata"
		     "font-gnu-unifont"
		     "font-public-sans"
		     "font-misc-misc"
		     "font-awesome"
		     "font-hack"
		     "font-iosevka"
		     "font-iosevka-term"
		     "font-iosevka-term-slab"
		     ;"font-iosevka-sparkle"
		     "font-iosevka-slab"
		     "font-iosevka-etoile"
		     "font-iosevka-aile"
		     "font-google-noto"))



(specifications->manifest `(,@specs
			    ,@misc-specs
			    ,@font-specs
			    ,@build-systems))
