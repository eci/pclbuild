(use-modules  (eci packages bioinformatics)
	      (eci packages cran)
	      (guix packages)
	      (gnu packages chemistry)
	      (gnu packages bioinformatics)
	      (gnu packages bioconductor)
	      (gnu packages statistics)
	      (gnu packages web)
	      (gnu packages image)
	      (gnu packages cran)
	      (gnu packages)
	      (srfi srfi-1))



(define misc-spec '(;; "openbabel"
		    "inchi"
		    "gcc-toolchain"
		    "openjdk"))

(define emacs-spec '("emacs-ess"
		     "emacs-poly-r"))


(define py-spec '("perl"
                  "python"
		  ;"pypy"
		  "python-pyyaml"
		  "python-requests"
		  "python-toml"
		  ;; "python-ruamel.yaml"
		  "python-mistletoe"))

(define r-spec `("r" "r-scales"
		 "r-devtools"
		 "r-openssl"
                 "r-foreach"
		 "r-markdown"
		 "r-rmarkdown"
                 "r-dofuture"
                 "r-ff"
                 "r-ffbase"
		 "r-yaml"
                 "r-batchtools"
		 "r-future"
		 "r-mzr"
		 "r-bit64"
		 "r-data-table"
		 "r-msnbase"
		 "r-assertthat"
		 "r-withr"
		 "r-ggplot2"
		 "r-cowplot"
		 "r-rcolorbrewer"
		 "r-curl"
		 "r-shiny"
		 "r-handsontable"
		 "r-dt"
		 "r-tcltk2"
		 "r-protgenerics"
		 "r-massbank"
		 "r-chemmass"
		 "r-resolution"
		 "r-r-utils"
                 "r-tinytex"
		 ))




(specifications->manifest `(,@misc-spec
			    ;; ,@emacs-spec
			    ,@py-spec
			    ,@r-spec
			    ))
