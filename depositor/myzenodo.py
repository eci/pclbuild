import json
import sys
import os
import os.path
import requests
import glob
import mistletoe
import re
import ruamel.yaml as yaml
from pathlib import Path
from mistletoe.ast_renderer import ASTRenderer
import sys
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import hashlib
# Not to annoy Zenodo excessively
session = requests.Session()
retry = Retry(connect=3, backoff_factor=0.5)
adapter = HTTPAdapter(max_retries=retry)
session.mount('http://', adapter)
session.mount('https://', adapter)

EXTS= ['.csv','.csv.gz','.csv.bz2','.csv.xz','.csv.zip']

def upd_semver_patch(meta,patch=None):
    semver=meta['version']
    things=semver.split('.')
    majr=things[0]
    minr=things[1]
    if not patch:
        new_patch=str(int(things[2])+1)
    else:
        new_patch=str(patch)

    meta['version']=majr+'.'+minr+'.'+new_patch
    return(meta)

def hash_bytestr_iter(bytesiter, hasher, ashexstr=True):
    # Source: SO, Q: 3431825 (generating-an-md5-checksum-of-a-file)
    for block in bytesiter:
        hasher.update(block)
    return hasher.hexdigest() if ashexstr else hasher.digest()

def file_as_blockiter(afile, blocksize=65536):
    # Source: SO, Q: 3431825 (generating-an-md5-checksum-of-a-file)
    with afile:
        block = afile.read(blocksize)
        while len(block) > 0:
            yield block
            block = afile.read(blocksize)

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


# def upload_a_file(meta,jdepo,depoconf,access_token,fn):
#     if jdepo['submitted']: #TODO
#     files_url=jdepo['links']['files']
#     dir_pub=depoconf['local']+"/publish"
#     zenodo_files= session.get(files_url,params={'access_token':access_token}).json()



def create_deposition(meta,depo_url,access_token):
    headers = {"Content-Type": "application/json"}
    params = {'access_token': access_token}
    eprint("Uploading to %s" % depo_url)
    meta=upd_semver_patch(meta,0) # X.Y.0, where X.Y comes from Major
                                  # Version heading.
    data = {'metadata':meta}
    r = session.post(url = depo_url,
                     params=params,
                     data=json.dumps(data),
                     # Headers are not necessary here since "requests" automatically
                     # adds "Content-Type: application/json", because we're using
                     # the "json=" keyword argument
                     # headers=headers,
                     headers=headers)

    if 399 < r.status_code and r.status_code < 500:
        eprint(r.json())
        sys.exit('Fatal error. Exit.')

    eprint("Deposition creation finished with exit code %s." % r.status_code)
    return(r.json())

def update_metadata(meta,depoconf,jdepo,access_token):

    if jdepo['submitted']:
        eprint('Updating metadata, if changed.')
        depoid=jdepo['id']
        eprint('depoid:'+str(depoid))
        url_zenodo=depoconf['url_zenodo']
        url = "%s/%s?access_token=%s" % (url_zenodo,depoid,access_token)
        headers = {"Content-Type": "application/json"}
        r = session.post('%s/%s/actions/edit' % (url_zenodo,depoid) ,
                         params={'access_token': access_token})
        if 399 < r.status_code and r.status_code < 500:
            eprint(r.json())
            sys.exit('Fatal error when editing. Exit.')

        r = session.put(url, data=json.dumps({'metadata':meta}), headers=headers)
        if 399 < r.status_code and r.status_code < 500:
            eprint(r.json())
            sys.exit('Fatal error when updating. Exit.')
        jdepo = r.json()
        r = session.post('%s/%s/actions/publish' % (url_zenodo,depoid),
                         params={'access_token': access_token})
        if 399 < r.status_code and r.status_code < 500:
            eprint(r.json())
            sys.exit('Fatal error when publishing update. Exit.')
            
        return(jdepo)
    else:
        eprint('Skipping metadata update for unpublished deposition.')
        return(jdepo)

    
def upload_data(meta,depoconf,jdepo,access_token):
    files_url=jdepo['links']['files']
    dir_pub=depoconf['local']+"/publish"
    zenodo_files= session.get(files_url,params={'access_token':access_token}).json()
    files={s:{'filename':s,
              'checksum':hash_bytestr_iter(file_as_blockiter(open(dir_pub+'/'+s, 'rb')),
                                           hashlib.md5())} for s in os.listdir(dir_pub) \
              if any([s.endswith(ext) for ext in EXTS])}
    zenodo_files={o['filename']:{'filename':o['filename'],'checksum':o['checksum']} for o in zenodo_files}

    fns_nonext=list(set(files.keys())-set(zenodo_files.keys()))
    fns_samename=list(set(files.keys())-set(fns_nonext))
    fns_refresh=[]
    for fn in fns_samename:
        loc_chk=files[fn]['checksum']
        rem_chk=zenodo_files[fn]['checksum']
        if loc_chk!=rem_chk:
            fns_refresh.append(fn)
            

    if (len(fns_refresh)!=0 or len(fns_nonext)!=0):
        if jdepo['submitted']:
             url_zenodo=depoconf['url_zenodo']
             depoid=jdepo['id']
             r = session.post('%s/%s/actions/newversion' % (url_zenodo,depoid),
                              params={'access_token': access_token})
             jdepo=r.json()
             depoid=jdepo['id']
        
        url_upl=files_url+'?access_token='+access_token
        for fn in fns_nonext:
            eprint('Commencing upload of file:'+fn)
            info={'name':fn}
            fls={'file':open(dir_pub+'/'+fn,'rb')}
            r=session.post(url=url_upl,
                           data=info,
                           files=fls)
            if r.status_code!=201:
                eprint(r.json())
                eprint('Fatal error when creating new Zenodo version.')
            if r.status_code!=201:
                eprint(r.json())
                eprint('Fatal file upload error.')

        for fn in fns_refresh:
            info={'name':fn}
            fls={'file':open(dir_pub+'/'+fn,'rb')}
            eprint('Updating file on Zenodo: '+fn)
            r=session.post(url=url_upl,
                           data=info,
                           files=fls)
            if r.status_code!=201:
                eprint(r.json())
                eprint('Fatal file update error.')
    else:
        eprint('No local files updated nor added, so no need to change files on Zenodo.')

    
    return(None)
    



def get_depo(depo_url,access_token,title):
    r = session.get(depo_url,
                     params={'q': title,
                             'sort':'mostrecent',
                             'size':1,
                             'access_token': access_token})

    if (len(r.json())==0):
        return(None)
    jdepo=r.json()[0]
    eprint(jdepo)
    fh=open('match.json','w')
    print(json.dumps(jdepo,indent=4),file=fh)
    fh.close()
    
    if jdepo['title'] != title:
        jdepo=None
    return(jdepo)

def parse_publish(fn_publish):

    # Remove MarkDown comments, otherwise they'll confuse the parser.
    text=re.sub("(<!--.*?-->)", "", Path(fn_publish).read_text(encoding="utf-8"), flags=re.DOTALL)

    # Return the md syntax tree in content.
    with ASTRenderer() as renderer:
        content=json.loads(renderer.render(mistletoe.Document(text)))['children']


    # Pick up things we need.
    meta={'description':"",
          'notes':"",
          'creators':[],
          'contributors':[],
          'references':[],
          'communities':[],
          'keywords':[],
          'related_identifiers':[],
          'upload_type':'dataset'}
    nextisdesc=False
    topic=None
    listtopic=None
    creator=None
    contributor=None
    for item in content:
        if item.get('level')==1:
            meta['title']=item['children'][0]['content']

        elif item.get('type')=='Heading' and \
             item.get('level')==2:
            topic=item.get('children')[0]['content']
            topic=topic.lower()

        elif topic=='description':
            if item.get('type')=='Paragraph':
                for txt in item.get('children'):
                    if txt.get('type')=='RawText':
                        meta['description']+=txt['content']
                    elif txt.get('type')=='LineBreak':
                        meta['description']+="\n"
                    elif txt.get('type')=='Strong':
                        meta['description']+='**'+txt.get('children')[0].get('content')+'**'
                    elif txt.get('type')=='Emphasis':
                        meta['description']+='*'+txt.get('children')[0].get('content')+'*'
                    elif txt.get('type')=='Link':
                        meta['description']+='['+txt.get('children')[0].get('content')+']' +\
                        '('+txt.get('target')+')'
                    else:
                        meta['description']+="____UNRECOGNISEDTOKEN____"
            meta['description']+="\n" # Divide paragraphs.

        elif topic=='notes':
            if item.get('type')=='Paragraph':
                for txt in item.get('children'):
                    if txt.get('type')=='RawText':
                        meta['notes']+=txt['content']
                    elif txt.get('type')=='LineBreak':
                        meta['notes']+="\n"
                    elif txt.get('type')=='Strong':
                        meta['notes']+='**'+txt.get('children')[0].get('content')+'**'
                    elif txt.get('type')=='Emphasis':
                        meta['notes']+='*'+txt.get('children')[0].get('content')+'*'
                    elif txt.get('type')=='Link':
                        meta['notes']+='['+txt.get('children')[0].get('content')+']' +\
                        '('+txt.get('target')+')'
                    else:
                        meta['notes']+="____UNRECOGNISEDTOKEN____"
            meta['notes']+="\n" # Divide paragraphs.

        elif topic=='creators':
            if item.get('type')=='Heading' and \
               item.get('level')==3:
                name=item.get('children')[0]['content']
                creator={'name':name}
            elif item.get('type')=='List' and creator:
                for li in item.get('children'):
                    for lichild in li.get('children'):
                        keyvals=lichild['children']
                        for thing in keyvals:
                            if thing['type']=='InlineCode':
                                key=thing['children'][0]['content']
                                creator[key]=None
                            elif thing['type']=='RawText':
                                val=thing['content'].strip()
                                creator[key]=val
                meta['creators'].append(creator)
                creator=None
                
        elif topic=='contributors':
            if item.get('type')=='Heading' and \
               item.get('level')==3:
                name=item.get('children')[0]['content']
                contributor={'name':name}
            elif item.get('type')=='List' and contributor:
                for li in item.get('children'):
                    for lichild in li.get('children'):
                        keyvals=lichild['children']
                        for thing in keyvals:
                            if thing['type']=='InlineCode':
                                key=thing['children'][0]['content']
                                contributor[key]=None
                            elif thing['type']=='RawText':
                                val=thing['content'].strip()
                                contributor[key]=val
                meta['contributors'].append(contributor)
                contributor=None

        elif topic=='references':
            if item.get('type')=='List':
                for li in item.get('children'):
                    ref=li['children'][0]['children'][0]['content']
                    meta['references'].append(ref)

        elif topic=='communities':
            if item.get('type')=='List':
                for li in item.get('children'):
                    ref=li['children'][0]['children'][0]['content']
                    meta['communities'].append({'identifier':ref.strip()})

        elif topic=='keywords':
            if item.get('type')=='List':
                for li in item.get('children'):
                    ref=li['children'][0]['children'][0]['content']
                    meta['keywords'].append(ref)

        
        elif topic=='related identifiers':
            if item.get('type')=='Heading' and \
               item.get('level')==3:
                name=item.get('children')[0]['content']
                relident={}
            elif item.get('type')=='List':
                for li in item.get('children'):
                    for lichild in li.get('children'):
                        keyvals=lichild['children']
                        for thing in keyvals:
                            if thing['type']=='InlineCode':
                                key=thing['children'][0]['content']
                                relident[key]=None
                            elif thing['type']=='RawText':
                                val=thing['content'].strip()
                                relident[key]=val
                meta['related_identifiers'].append(relident)
                relident=None

        elif topic=='major version':
            if item.get('type')=='Paragraph':
                meta['version']= item.get('children')[0]['content']

        
                    



    if (len(meta['communities'])==0):
        meta.pop('communities',None)

    # if (len(meta['related_identifiers'])==0):
    #     meta.pop('related_identifiers',None)
    
    return(meta,content)
