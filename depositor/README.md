# Depositor

Depositor is used to upload MetFrag DBs to Zenodo.

## Setup

To run Depositor, it is necessary to create a top-level
data/configuration directory (in further text `depositor`). This
directory must contain a subdirectory called `publish` as well as a
Markdown file called `publish.md`. In addition, file `input.yaml` 
must exist.

### `input.yaml`
It contains a path to the `depositor` directory on the server. It 
also defines which file prefixes in `publish` should be monitored 
for upload.

### `publish`
The `publish` subdirectory should contain the files which are to be
uploaded. Each time the program is run and allowed to make changes,
the files which match monitored file prefixes will be compared against
those in the repository and uploaded if different _and_ newer.

File `publish.md` is used to specify deposition metadata. It must
follow a structure outlined in the example below. Every time Depositor
runs, it will update the metadata if there are any differences.

### Versioning
Deposition versions are only updated if there are changes in the
**content** of files under `publish` directory. Changing metadata does
not update the version.

### Example input.yaml
```yaml
depositor:
  local: /path/to/depositor/directory
  url_zenodo: 'https://sandbox.zenodo.org/api/deposit/depositions'
  # url_zenodo: 'https://zenodo.org/api/deposit/depositions'
  monitor:
    - prefix_1_
    - prefix_2_
  

```
### Example publish.md

```markdown
<!-- Title of the deposition. -->
# Good Night World

<!-- Description: Level two heading under which follows the
description text. -->
## Description

This deposition is best described in the following terms... 
extending the sentence here, just so that it is obvious that 
one can add paragraph(s) of text.

<!-- Creators: Level three headings containing an author's name. Under
the heading, a key-value list such as: 
### Doe, John

- `affiliation` Independent Scientists Unlimited
- `orcid` 0000-0000-0000-0000


The list item sign `-' should be at the beginning of the line.  -->
## Creators

### Mustermann, Muster ibn Mussa
- `affiliation` Oolong Uni

<!-- Contributors: Level three headings containing an contributor's
name. Under the heading, a key-value list such as:
### Doe, John

- `affiliation` Independent Scientists Unlimited
- `orcid` 0000-0000-0000-0000
- `type` Distributor

The `type` item should be one of Zenodo's contributor types.

The list item sign `-' should be at the beginning of the line.  -->
## Contributors

<!-- Similar to `Description' heading. -->
## Notes

Some notes here.


<!-- References as a list.  -->
## References

- Doe, J., Mimibobo, 2021.

<!-- Related identifiers similar to contributors...  -->
## Related identifiers

### Vivo
- `identifier` https://example.com
- `relation` isSupplementTo

<!-- Communities as a list. -->
## Communities
- my-existing-zenodo-community
- my-other-existing-zenodo-community

<!-- Keywords as a list. -->
## Keywords

- key
- word
- s

<!-- Just the major version. The program will compute
the rest of the semantic versioning scheme. -->
## Major Version
3

## License
cc-by

```


## Run

The bash script `pubchem/pubchemlite/depositor/run-depositor` takes
care of running the program. It has to be run in an environment where
the following environment variables have been defined,

- `PCL_MANIF`, path to `input.yaml` 
- `PCL_ZENODO_TOKEN`, contains the Zenodo access token

### Example command line
```shell
PCL_MANIF=/path/to/input.yaml PCL_ZENODO_TOKEN=$(cat /path/to/the/access/token.txt) /path/to/pubchem/repository/pubchem/pubchemlite/depositor/run-depositor
```


## How it works

1. Is there a file named `DONOTRUN` in the `depositor`  
   directory? If no, proceed, otherwise exit.
2. Is there an already saved deposition id in 
   `depositor/depid`? If yes, work with that deposition, 
   otherwise search for the title from `publish.md`. If a 
   deposition with that title has been found, use it. If not, 
   create new deposition.
3. Compare the data in `publish` to files in the deposition. Files with
   monitored prefixes are uploaded if their datestamp (...YYYYMMDD.csv)
   is newer, or --- if the datestamps are identical --- if the
   contents are different.
4. If any files have been changed, the minor version of the deposition is
   increased by one.
5. Any changes to `publish.md` are also uploaded, but they will not cause
   a version bump.
6. Create `DONOTRUN` file in the `depositor` directory. This will preempt
   depositor running commiting things inadvertently. 

Whenever new files are ready for upload in `publish`, it is up to the user
to delete `DONOTRUN` file in `depositor`, so that the program will run.

## In case of a faulty submission

Upload a fixed file, then manually increase the patch 
part of the version string(X.Y.patch+1).
