#!/usr/bin/perl

use strict;
use warnings;

my $prefix = $ARGV[0];
if ( ! defined( $prefix ) ) {
  $prefix = "";
}

#5       HIQNVODXENYOFK  01001000        3       7       5 25244426      3-Amino-2-oxopropyl phosphate   C3H8NO5P    C(C(=O)COP(=O)(O)O)N     InChI=1S/C3H8NO5P/c4-1-3(5)2-9-10(6,7)8/h1-2,4H2,(H2,6,7,8)     HIQNVODXENYOFK-UHFFFAOYSA-N 169.014009       (3-amino-2-oxopropyl) dihydrogen phosphate
#6       VYZAHLCBVHPDDF  01101111        3101    6797    6       1-chloro-2,4-dinitrobenzene     C6H3ClN2O4      C1=CC(=C(C=C1[N+](=O)[O-])[N+](=O)[O-])Cl    InChI=1S/C6H3ClN2O4/c7-5-2-1-4(8(10)11)3-6(5)9(12)13/h1-3H      VYZAHLCBVHPDDF-UHFFFAOYSA-N  201.978134      1-chloro-2,4-dinitrobenzene

#249     SXKNCCSPZDCRFD  01001001        203     874     249     betaine aldehyde        C5H12NO+        C[N+](C)(C)CC=O InChI=1S/C5H12NO/c1-6(2,3)4-5-7/h5H,4H2,1-3H3/q+1       SXKNCCSPZDCRFD-UHFFFAOYSA-N     102.091889      trimethyl(2-oxoethyl)azanium
#259     CPELXLSAUQHCOX  01101111        120     15428   259 260 518779 22169024 bromide Br-     [Br-]   InChI=1S/BrH/h1H/p-1    CPELXLSAUQHCOX-UHFFFAOYSA-M     78.91834        bromide

my $ipc = 0;  # "Ideal" parent CID  (most annotated exemplar, after considering all alternative forms)
# All reduced by one as FP column was eliminated
my $icd = 4;  # Assoc. CID list
my $imf = 6;  # Mol. Formula
my $iis = 7;  # SMILES
my $iin = 8;  # InChI
my $iim = 10; # Isotopic Mass
my $ilp = 11; # XLogP

my $smiles_file = $prefix . "cases_multicomponent.tsv";
my $smile2_file = $prefix . "cases_mixture.tsv";
my $element_file = $prefix . "cases_badelement.tsv";
my $charged_file = $prefix . "cases_charged.tsv";
my $light_file = $prefix . "cases_light.tsv";

open( SMI, "> $smiles_file" ) || die "Unable to write $smiles_file!\n";
open( SM2, "> $smile2_file" ) || die "Unable to write $smile2_file!\n";
open( ELE, "> $element_file" ) || die "Unable to write $element_file!\n";
#open( CHG, "> $charged_file" ) || die "Unabel to write $charged_file!\n";
#open( LGH, "> $light_file" ) || die "Unabel to write $light_file!\n";

my $nsmi = 0;
my $nsm2 = 0;
my $nele = 0;
my $nchg = 0;
my $nlgh = 0;
my $nok = 0;
my $ntot = 0;


# Check for fingerprint bit string (using numeric properties now but is kept for validation purposes in earlier processing)
my $ifp = -1;
$_ = <STDIN>;
chop;
$_ =~ s/\"/\"\"/g; # CSV encoding handling
{
  my @tmp = split( /	/, $_ );
  my $ntmp = @tmp;
  for ( my $i = 0;  $i < $ntmp;  $i++ ) {
    if ( $tmp[$i] eq "FP" ) {
      $ifp = $i;
      last;
    }
  }

  if ( $ifp != -1 ) {
    splice( @tmp, $ifp, 1 );  # Shorten the array by one and eliminate the string fingerprint
  } else {
    print STDERR "WARNING :: FP column not detected .. expecting it to be \"$tmp[2]\" from:\n$_\n";
  }

  print "\"", join( "\",\"", @tmp ), "\"\n";

  if ( $ifp != 2 ) {
    print STDERR "WARNING :: string fingerprint is expected in the 3rd column .. output may be corrupt!\n";
  }
}
#print $_;

while ( $_ = <STDIN> ) {
  chop;
  $_ =~ s/\"/\"\"/g; # CSV encoding handling

#  my ( $pcid, $ifb, $fp, $npmid, $nppid, $cids, $na, $mf, $is, $in, $ik, $im, $lp, $iu ) = split( /	/, $_, 14 );
  my @tmp = split( /	/, $_ );
  $ntot++;

  if ( $ifp != -1 ) {
    splice( @tmp, $ifp, 1 );  # Shorten the array by one and eliminate the string fingerprint
  }

  if ( $tmp[ $imf ] =~ /Kr|Dy|Ir|La|Lu|Nd|Nb|Os|Pd|Pt|Pu|Pr|Re|Rh|Ru|Sm|Sc|Ag|Ta|Tc|Tb|Th|Tm|Ti|W|Ac|Am|Er|Eu|Gd|Hf|Ho|Xe|Yb|Rn|Sr|Be|Cm|Cf|Cs|Md|Pm|Fr|Pa|Np|Bk|Es|Fm|No|Lr|Rf|Db|Sg|Bh|Hs|Mt|Ds|Rg|Cn|Nh|Fl|Mc|Lv|Ts|Og/ ) {
#    print "MF bad element\t$tmp[ $imf ]\t$_\n";
    print ELE "$_\n";
    $nele++;
  } elsif ( $tmp[ $iis ] =~ /\./ ) {
#    print "multicomponent smiles\t$tmp[ $iis ]\t$_\n";
    if ( $tmp[ $ipc ] ne $tmp[ $icd ] ) {
      print SM2 "$_\n";
      $nsm2++;
    } else {
      print SMI "$_\n";
      $nsmi++;
    }
#  } elsif ( $tmp[ $iim ] < 50.0 ) {
#    if ( $tmp[ $imf ] =~ /\-|\+/ ) {
#      print "$tmp[ $imf ] MF charged\t$_\n";
#      my ( $mf, $charge ) = split( /\-|\+/, $tmp[ $imf ] );
#      $tmp[ $imf ] = $mf;
#
#      print LGH join( "\t", @tmp ), "\n";
#    } else {
#      print LGH "$_\n";
#    }
#    $nlgh++;
  } elsif ( $tmp[ $imf ] =~ /\-|\+/ ) { 
#    print "$tmp[ $imf ] MF charged\t$_\n";
    my ( $mf, $charge ) = split( /\-|\+/, $tmp[ $imf ] );
#    print STDERR "before: $tmp[ $imf ] .. after: $mf\n";
    $tmp[ $imf ] = $mf;

#    print join( "\t", @tmp ), "\n";  # Charged files are okay
    print "\"", join( "\",\"", @tmp ), "\"\n";

#    print CHG join( "\t", @tmp ), "\n";
    $nchg++;
  } else {
    print "\"", join( "\",\"", @tmp ), "\"\n";
#    print "$_\n";
    $nok++;
  }
#  $u{ $pcid } = $_;
}
close( SMI );
close( SM2 );
close( ELE );
#close( CHG );
#close( LGH );

print STDERR "Read $ntot lines\n  BadElement: $nele\n  Multicomponent: $nsmi\n  Mixture (no parent): $nsm2\n  Charged (mod MF): $nchg\n  Okay: $nok (", ( $nok + $nchg ), ")\n";
#print STDERR "Read $ntot lines\n  BadElement: $nele\n  Multicomponent: $nsmi\n  Mixture (no parent): $nsm2\n  TooLight: $nlgh\n  Charged (mod MF): $nchg\n  Okay: $nok (", ( $nok + $nchg ), ")\n";


#sub bynum { $a <=> $b };

